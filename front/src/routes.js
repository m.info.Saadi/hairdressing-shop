import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/Login",
      component: function(resolve) {
        require(["@/components/LoginAndRegest/Login.vue"], resolve);
      },
    },
    {
      path: "/Regest",
      component: function(resolve) {
        require(["@/components/LoginAndRegest/Regest.vue"], resolve);
      },
    },
    {
      path: "/form/:idActualUser",
      component: function(resolve) {
        require(["@/components/Form/Formpage.vue"], resolve);
      },
    },
   
    {
      path: "/Service/:idActualUser",
      component: function(resolve) {
        require(["@/components/Service/Service.vue"], resolve);
      },
    },
    {
      path: "/Hairdressers",
      component: function(resolve) {
        require(["@/components/Hairdressers/Hairdressers.vue"], resolve);
      },
    },
    {
      path: "/",
      component: function(resolve) {
        require(["@/components/Header/Header.vue"], resolve);
      },
    },
    {
      path: "/Reservation/:idActualUser",
      component: function(resolve) {
        require(["@/components/Reservation/ReservationList.vue"], resolve);
      },
    },
  ],
  /*const router = new VueRouter({
  mode: "history",
  base: process.env.BASE.URL,
  routes,*/
});
export default router;
