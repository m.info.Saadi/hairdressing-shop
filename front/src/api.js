//onst roote = "/api";
import { v4 as uuidv4 } from 'uuid'
export default {
 // accessToken: "",
  root: "/api",
/*
  async newRegestrar(user) {
    await this._fetch("POST", "/register", {
      name_user: user.name_user,
      lastname_user: user.lastname_user,
      sex: user.sex,
      phone: user.phone,
      email: user.email,
      pasword: user.pasword,
    }).then((response) => response.json());
  },

  async _fetch(method, endpoint, data) {
    const body = method === "POST" ? JSON.stringify(data) : undefined;
    const headers = {
      "Content-type": "application/json",
      Authorization: "Bearer " + this.accessToken,
    };

    const response = await fetch(this.root + endpoint, {
      method,
      headers,
      body,
    });

    console.dir(response);

    if (response.status < 300) {
      return {
        status: response.status,
        data: await response.json(),
      };
    } else {
      return {
        status: response.status,
        error: "unauthorized",
      };
    }
  },

  async login(email, pasword) {
    const response = await this._fetch("POST", "/login", {
      email,
      pasword,
    });
    const ok = response.status == 200;
    const data = response.data;
    const error = "authentication error";

    if (ok) {
      console.log("ok", data);
      this.accessToken = data.accessToken;
      return { ok, data };
    } else {
      console.log("no ok");
      this.accessToken = "";
      return { ok, error };
    }
  },
*/
  async displayApiReservation(id_user) {
    let result = await fetch(this.root + "/displayReservation/"+id_user);
    return await result.json();
  },
  async displayServices() {
    const result = await fetch(this.root +'/service');
    return await result.json();
  },
  async displayHairdressers() {
    const result = await fetch(this.root +'/Hairdressers');
    return await result.json();
  },
  
/*  async displayHours(id_hairdressers, datee) {
    const result = await fetch(this.root +'/reservation/'+id_hairdressers+'/'+datee);
    return await result.json();
  },*/

  async displayHourstocuthair(id_hairdressers, datee) {
    const result = await fetch(this.root +'/reservationtocuthair/'+id_hairdressers+'/'+datee);
    return await result.json();
  },
  async displayHourscutbeard(id_hairdressers, datee) {
    const result = await fetch(this.root +'/reservationcutbeard/'+id_hairdressers+'/'+datee);
    return await result.json();
  },
  async displayHourscutbeardandhair(id_hairdressers, datee) {
    const result = await fetch(this.root +'/reservationcutbeardandhair/'+id_hairdressers+'/'+datee);
    return await result.json();
  },

  async reservationendtimecutbeard(id_hairdressers, datee,timee) {
    const result = await fetch(this.root +'/reservationendtimecutbeard/'+id_hairdressers+'/'+datee+'/'+timee);
    return await result.json();
  },

  async reservationendtimecuthair(id_hairdressers, datee,timee) {
    const result = await fetch(this.root +'/reservationendtimecuthair/'+id_hairdressers+'/'+datee+'/'+timee);
    return await result.json();
  },

  async reservationendtimecuthairandbeard(id_hairdressers, datee,timee) {
    const result = await fetch(this.root +'/reservationendtimecuthairandbeard/'+id_hairdressers+'/'+datee+'/'+timee);
    return await result.json();
  },

  async displaydateReservation(id_user) {
    const result = await fetch(this.root +'/displaydateReservation/'+id_user);
    return await result.json();
  },

  async deletresrvation(id_reservation) {
    await fetch(this.root+"/deletresrvation/" + id_reservation, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
  },





  async sendResvation(reservation) {
    await fetch(this.root+"/saveReservationdata", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id_reservation:uuidv4() ,
          id_user: reservation.id_user,
          day_week:reservation.day_week,
          datee:reservation.datee,
          timee:reservation.timee,
          id_hairdressers:reservation.id_hairdressers,
          id_service:reservation.id_service,
          end_time:reservation.end_time
        }),
    }).then(response => response.json())
},
};
