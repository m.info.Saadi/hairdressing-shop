<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Collection;
use ReallySimpleJWT\Token;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::POST('/register', function () {
   
    $data = request()->all();
    DB::insert(
        'INSERT into users(name_user,lastname_user,sex,phone,email,pasword) VALUES
         (:name_user,:lastname_user,:sex,:phone,:email,:pasword)',
        $data
    );
   
    $results = DB::select('select * from users where name_user = :name_user', [
        'name_user' => $data['name_user'],
    ]);
    return response()->json($results[0], 200);
});


Route::post('/login/{email}/{pasword}', function () {
    $data = request()->all();

    // en $data hay claves 'email' y 'password'
    // se comprueba que el usuario es correcto

    if (
        $reponse = DB::select ('select * from users where email=:email and pasword=:pasword',
        [
            'email' =>$data['email'],
            'pasword'=>$data['pasword'],
         ])
        ($data['mail'] == 'med95@gmail.com' && $data['password'] == '123456')
        || ($data['mail'] == 'filemon@tia.es' && $data['password'] == 'bacterio')
        || ($data['mail'] == 'vicente@tia.es' && $data['password'] == 'sacarino')
    ) {
        $payload = [
            'email' => $data['email'],
            'role' => getRole($data['email']),
        ];
        $secret = getSecret();
        $token = Token::customPayLoad($payload, $secret);
        return response()->json([
            'accessToken' => $token,
        ], 200);
    }

    return response()->json([], 401);
});

if (!function_exists('getRole')) {
    function getRole($email)
    {
        if ($email === $reponse) {
            return 'superintendente';
        }
        return 'agente';
    }
}
if (!function_exists('getSecret')) {
    function getSecret()
    {
        return 'sUper!secret&key·123';
    }
}
*/
///////////////////////////////////////////////////////////////////



/*Route::get('/reservation/{id_hairdressers}/{datee}', function ($id_hairdressers,$datee) {
    
    $results = DB::select('select *  from week_calendar  as a
    left join
    (SELECT * from reservation where    id_hairdressers=:id_hairdressers and datee=:datee   ) as b
    on
    a.day_week=b.day_week AND
    a.id_hairdressers=b.id_hairdressers AND
    a.time=b.timee
    where a.id_hairdressers=:id_hairdressers
    and id_reservation is null ', [
        'id_hairdressers' => $id_hairdressers,
        'datee' => $datee,
      
    
    ]);
    return response()->json($results, 200);
});
*/
Route::get('/reservationtocuthair/{id_hairdressers}/{datee}', function ($id_hairdressers,$datee) {
    
    $results = DB::select('select a.day_week, a.time,b.end_time, a.id_hairdressers from(
        select a.day_week, a.time,b.end_time, a.id_hairdressers from
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers
        and id_reservation is null) as a
        join
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers
        and id_reservation is null) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.end_time=b.time) as a
        JOIN
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers
        and id_reservation is null) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.end_time=b.time', [
        'id_hairdressers' => $id_hairdressers,
        'datee' => $datee,
      
    
    ]);
    return response()->json($results, 200);
});



Route::get('/reservationcutbeard/{id_hairdressers}/{datee}', function ($id_hairdressers,$datee) {
    
    $results = DB::select('select a.day_week, a.time,b.end_time, a.id_hairdressers from
    (select *  from week_calendar  as a
    left join
    (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
    on
    a.day_week=b.day_week AND
    a.id_hairdressers=b.id_hairdressers AND
    a.time=b.timee
    where a.id_hairdressers=:id_hairdressers
    and id_reservation is null) as a
    join
    (select *  from week_calendar  as a
    left join
    (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
    on
    a.day_week=b.day_week AND
    a.id_hairdressers=b.id_hairdressers AND
    a.time=b.timee
    where a.id_hairdressers=:id_hairdressers
    and id_reservation is null) as b
    on
    a.day_week=b.day_week AND
    a.id_hairdressers=b.id_hairdressers AND
    a.end_time=b.time', [
        'id_hairdressers' => $id_hairdressers,
        'datee' => $datee,
      
    
    ]);
    return response()->json($results, 200);
});




Route::get('/reservationcutbeardandhair/{id_hairdressers}/{datee}', function ($id_hairdressers,$datee) {
    
    $results = DB::select('select a.day_week, a.time,b.end_time, a.id_hairdressers from(
        select a.day_week, a.time,b.end_time, a.id_hairdressers from(
        select a.day_week, a.time,b.end_time, a.id_hairdressers from
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers 
        and id_reservation is null) as a
        join
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers  and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers 
        and id_reservation is null) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.end_time=b.time) as a
        JOIN
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers  and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers 
        and id_reservation is null) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.end_time=b.time)as a
        JOIN
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers  and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers 
        and id_reservation is null) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.end_time=b.time', [
        'id_hairdressers' => $id_hairdressers,
        'datee' => $datee,
      
    
    ]);
    return response()->json($results, 200);
});

/**************************************************************************************************** */



Route::get('/reservationendtimecuthair/{id_hairdressers}/{datee}/{timee}', function ($id_hairdressers,$datee,$timee) {
    
    $results = DB::select('select a.day_week ,a.time,b.end_time,a.id_hairdressers from(
        select a.day_week, a.time,b.end_time, a.id_hairdressers from
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers
        and id_reservation is null) as a
        join
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers="1" and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers
        and id_reservation is null) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.end_time=b.time) as a
        JOIN
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers
        and id_reservation is null) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.end_time=b.time
        AND a.time=:timee', [
            'id_hairdressers' => $id_hairdressers,
            'datee' => $datee,
            'timee'=>$timee
    
    ]);
    return response()->json($results, 200);
});








Route::get('/reservationendtimecutbeard/{id_hairdressers}/{datee}/{timee}', function ($id_hairdressers,$datee,$timee) {
    
    $results = DB::select('select a.time,b.end_time from
    (select *  from week_calendar  as a
    left join
    (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
    on
    a.day_week=b.day_week AND
    a.id_hairdressers=b.id_hairdressers AND
    a.time=b.timee
    where a.id_hairdressers=:id_hairdressers
    and id_reservation is null) as a
    join
    (select *  from week_calendar  as a
    left join
    (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
    on
    a.day_week=b.day_week AND
    a.id_hairdressers=b.id_hairdressers AND
    a.time=b.timee
    where a.id_hairdressers=:id_hairdressers
    and id_reservation is null) as b
    on
    a.day_week=b.day_week AND
    a.id_hairdressers=b.id_hairdressers AND
    a.end_time=b.time 
    and a.time=:timee', [
        'id_hairdressers' => $id_hairdressers,
        'datee' => $datee,
        'timee'=>$timee
      
    
    ]);
    return response()->json($results, 200);
});





Route::get('/reservationendtimecuthairandbeard/{id_hairdressers}/{datee}/{timee}', function ($id_hairdressers,$datee,$timee) {
    
    $results = DB::select('select a.day_week, a.time,b.end_time, a.id_hairdressers from(
        select a.day_week, a.time,b.end_time, a.id_hairdressers from(
        select a.day_week, a.time,b.end_time, a.id_hairdressers from
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers
        and id_reservation is null) as a
        join
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers
        and id_reservation is null) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.end_time=b.time) as a
        JOIN
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers=:id_hairdressers and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers
        and id_reservation is null) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.end_time=b.time)as a
        JOIN
        (select *  from week_calendar  as a
        left join
        (SELECT * from reservation where id_hairdressers="1" and datee=:datee ) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.time=b.timee
        where a.id_hairdressers=:id_hairdressers
        and id_reservation is null) as b
        on
        a.day_week=b.day_week AND
        a.id_hairdressers=b.id_hairdressers AND
        a.end_time=b.time 
        AND a.time=:timee', [
        'id_hairdressers' => $id_hairdressers,
        'datee' => $datee,
        'timee'=>$timee
      
    
    ]);
    return response()->json($results, 200);
});






Route::get('/displaydateReservation/{id_user}', function ($id_user) {
    $results = DB::select('SELECT datee FROM reservation WHERE id_user=:id_user GROUP by datee ', [
        'id_user' => $id_user,
    
    ]);
    return response()->json($results, 200);
});




Route::get('/displayReservation/{id_user}', function ($id_user) {
    
    $results = DB::select('SELECT r.id_reservation, r.day_week,r.datee, (min(r.timee)) as timee,(max(r.end_time))as end_time,s.name,h.name_Hairdressers,h.lastname_Hairdressers from
     reservation r ,type_of_service s,Hairdressers h WHERE r.id_service=s.id_service AND h.id_hairdressers=h.id_hairdressers AND   r.id_user=:id_user GROUP by r.datee ', [
        'id_user' => $id_user,
     
      
    
    ]);
    return response()->json($results, 200);
});


Route::delete('/deletresrvation/{id_reservation}', function ($id_reservation) {
    $data = request()->all();
    DB::delete('delete from reservation where id_reservation=:id_reservation', [
          'id_reservation' => $id_reservation,
      ]);
  
     $results= [ 
      'succesful' => 'reservation deleted',
  ];
     
      return response()->json($results, 200);
  });






Route::get('/service', function (Request $request) {
    $results = DB::select('select * from type_of_service');
    return response()->json($results, 200);
});


Route::get('/Hairdressers', function (Request $request) {
    $results = DB::select('select * from Hairdressers');
    return response()->json($results, 200);
});




Route::post('/saveReservationdata', function () {

    $data = request()->all();
    $day_week=strtotime($data['datee']);
var_dump("datemokha".$day_week);
    DB::insert(
        "
        INSERT INTO reservation (day_week,timee,id_hairdressers,end_time,id_reservation,id_user,datee,id_service)
SELECT day_week,time,id_hairdressers,end_time,:id_reservation,:id_user,:datee,:id_service FROM week_calendar WHERE time>=:timee
AND end_time<=:end_time AND id_hairdressers=:id_hairdressers AND day_week=:day_week
    ",
        $data
    );
    $results = DB::select('select * from reservation where id_reservation = :id_reservation', [
        'id_reservation' => $data['id_reservation'],
    ]);
    return response()->json($results[0], 200);
});